#include "datetime.h"

int& DateTime::Get_minute() { return minute; }
int& DateTime::Get_second() { return second; }
int& DateTime::Get_hour() { return hour; }
int& DateTime::Get_day() { return day; }
int& DateTime::Get_month() { return month; }
int& DateTime::Get_year() { return year; }

std::ostream& operator << (std::ostream& stream, const DateTime& dt)
{
    stream << dt.hour << ' ' << dt.minute << ' ' << dt.second << ' ' << dt.day << ' ' << dt.month << ' ' << dt.year << std::endl;
    return stream;
}

std::istream& operator >> (std::istream& stream, DateTime& dt)
{
    stream >> dt.hour >> dt.minute >> dt.second >> dt.day >> dt.month >> dt.year;
    return stream;
}

time_t DateTime::Get_unix_time(DateTime A)
{
    tm timeinfo;
    timeinfo.tm_year = A.year - 1900;
    timeinfo.tm_mon = A.month - 1;
    timeinfo.tm_mday = A.day;
    timeinfo.tm_hour = A.hour;
    timeinfo.tm_min = A.minute;
    timeinfo.tm_sec = A.second;
    time_t unix_time = mktime(&timeinfo);
    return unix_time;
}

void DateTime::System_time(DateTime& system)
{
    time_t rawtime;
    tm timeinfo;
    time(&rawtime);
    localtime_s(&timeinfo, &rawtime);
    system.hour = timeinfo.tm_hour;
    system.minute = timeinfo.tm_min;
    system.second = timeinfo.tm_sec;
    system.day = timeinfo.tm_mday;
    system.month = timeinfo.tm_mon + 1;
    system.year = timeinfo.tm_year + 1900;
}

void DateTime::Get_normal_time(time_t unix_time, DateTime& A)
{
    tm timeinfo;
    localtime_s(&timeinfo, &unix_time);
    A.hour = timeinfo.tm_hour;
    A.minute = timeinfo.tm_min;
    A.second = timeinfo.tm_sec;
    A.day = timeinfo.tm_mday;
    A.month = timeinfo.tm_mon + 1;
    A.year = timeinfo.tm_year + 1900;
}

void DateTime::Get_next_date(DateTime& next)
{
    int min = 2419200 * 2;
    int max = 31536000;
    Get_normal_time(Get_unix_time(*this) + (min + rand() % (max - min + 1)), next);
}

void DateTime::Get_prev_date(DateTime& prev)
{
    int min = 2419200 * 2;
    int max = 31536000;
    Get_normal_time(Get_unix_time(*this) - (min + rand() % (max - min + 1)), prev);
}